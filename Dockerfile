FROM python:3.10

WORKDIR /code

RUN pip install --upgrade pip
RUN pip install poetry

COPY pyproject.toml /code
COPY poetry.lock /code
COPY setup.py /code
COPY .env /code

COPY src /code/src

RUN poetry config virtualenvs.create false \
  && poetry install $(test "$YOUR_ENV" = production && echo "--no-dev") --no-interaction --no-ansi

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "80"]
