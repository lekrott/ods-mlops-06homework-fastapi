"""
Catboost model FastAPI handler
"""
from fastapi import HTTPException
from dotenv import load_dotenv

from src.files import upload_to_pd
from src.model import EstimatorModel


load_dotenv()

catboost_model = EstimatorModel('churn.catboost', 'None')


async def catboost_upload_and_predict(file):
    """
    Catboost handler

    Args:
        file (UploadFile, optional): file to load input data from.

    Raises:
        HTTPException: risen if file extension is not CSV
    """
    if file.filename.endswith('.csv'):
        data = upload_to_pd(file)
    else:
        raise HTTPException(status_code=400, detail='Service only supports CSV files')

    return catboost_model.predict(data).tolist()
