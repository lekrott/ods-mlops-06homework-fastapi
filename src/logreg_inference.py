"""
Logreg model FastAPI handler
"""
from fastapi import HTTPException
from dotenv import load_dotenv

from src.files import upload_to_pd
from src.model import EstimatorModel


load_dotenv()

logreg_model = EstimatorModel('churn.logreg', 'None')


async def logreg_upload_and_predict(file):
    """
    Logreg handler

    Args:
        file (UploadFile, optional): file to load input data from.

    Raises:
        HTTPException: risen if file extension is not CSV
    """
    if file.filename.endswith('.csv'):
        data = upload_to_pd(file)
    else:
        raise HTTPException(status_code=400, detail='Service only supports CSV files')

    return logreg_model.predict(data).tolist()
