"""
Estimator model class
"""
import mlflow


# pylint: disable=R0903
class EstimatorModel:
    """
    Logreg model FastAPI wrapper
    """
    def __init__(self, model_name, model_stage):
        model_path = f'models:/{model_name}/{model_stage}'
        self.model = mlflow.pyfunc.load_model(model_path)

    def predict(self, data):
        """
        Returns prediction for given model and input data

        Args:
            data (pd.DataFrame): features to get predictions for

        Returns:
            pd.Series: target predictions for given features input
        """
        predictions = self.model.predict(data)
        return predictions
