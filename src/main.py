"""
Entry point for FastAPI service
"""
from fastapi import FastAPI, File, UploadFile

import src.logreg_inference
import src.catboost_inference


app = FastAPI()


# pylint: disable=C0116
@app.post('/logreg')
async def logreg_handler(file: UploadFile = File(...)):
    return await src.logreg_inference.logreg_upload_and_predict(file)


@app.post('/catboost')
async def catboost_handler(file: UploadFile = File(...)):
    return await src.catboost_inference.catboost_upload_and_predict(file)
