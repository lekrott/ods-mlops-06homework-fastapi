"""
File operations
"""
import os
import pandas as pd


def upload_to_pd(file):
    """Uploads file to pandas DataFrame

    Args:
        file (File): file to upload data from

    Returns:
        pd.DataFrame: dataframe with data from uploaded file
    """
    with open(file.filename, 'wb') as local_csv:
        local_csv.write(file.file.read())

    data = pd.read_csv(file.filename)
    os.remove(file.filename)

    return data
